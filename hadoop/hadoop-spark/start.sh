#!/bin/bash
set -e
confd --backend env --onetime
exec "$@"
