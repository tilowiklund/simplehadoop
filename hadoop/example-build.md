This corresponds to Databricks runtime 5.5 (DataBricks Runtime 5.4 Equivalent)
```
docker build -t tilowiklund/ubuntu-confd:16.04-v0.16.0 --build-arg UBUNTU_VERSION=16.04 base
docker build -t tilowiklund/simplehadoop:dbr55e --build-arg UBUNTU_VERSION=16.04 --build-arg SPARK_VERSION=2.4.3 --build-arg HADOOP_VERSION=2.7.3 hadoop-spark
```
