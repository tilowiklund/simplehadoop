#!/bin/bash
set -e
confd --backend env --onetime
if [ ! -f /hadoop_tmp/dfs/name/current/VERSION ]
then
  hdfs namenode -format
fi
hdfs namenode
