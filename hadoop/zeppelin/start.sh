#!/bin/bash
set -e
confd --backend env --onetime
zeppelin-daemon.sh start
tail --pid=$(pgrep java) -f /dev/null
